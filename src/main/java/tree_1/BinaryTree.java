/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tree_1;

/**
 *
 * @author djeq
 */
public class BinaryTree {

    Node root;

    public void add(int data) {
        root = add(root, data);
    }

    private Node add(Node current, int data) {
        if (current == null) {
            return new Node(data);
        }
        if (data < current.data) {
            current.kiri = add(current.kiri, data);
        } else if (data > current.data) {
            current.kanan = add(current.kanan, data);
        } else {
            return current;
        }
        return current;
    }

    public void delete(int data) {
        root = delete(root, data);
    }

    private Node delete(Node current, int data) {
        if (current == null) {
            return null;
        }

        if (data == current.data) {
            // Case 1: no children
            if (current.kiri == null && current.kanan == null) {
                return null;
            }

            // Case 2: only 1 child
            if (current.kanan == null) {
                return current.kiri;
            }

            if (current.kiri == null) {
                return current.kanan;
            }
            // Case 3: 2 children
            int smallestValue = findSmallestValue(current.kanan);
            current.data = smallestValue;
            current.kanan = delete(current.kanan, smallestValue);
            return current;
        }
        if (data < current.data) {
            current.kiri = delete(current.kiri, data);
            return current;
        }

        current.kanan = delete(current.kanan, data);
        return current;
    }

    private int findSmallestValue(Node root) {
        return root.kiri == null ? root.data : findSmallestValue(root.kiri);
    }
}
