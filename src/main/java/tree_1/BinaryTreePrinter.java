/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tree_1;

import java.io.PrintStream;

/**
 *
 * @author djeq
 */
public class BinaryTreePrinter {

    Node root;

    public BinaryTreePrinter(Node root) {
        this.root = root;
    }

    private String traversePreOrder(Node root) {

        if (root == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(root.data);

        String pointerRight = "└──(ka)";
        String pointerLeft = (root.kanan != null) ? "├──(ki)" : "└──(ki)";

        traverseNodes(sb, "", pointerLeft, root.kiri, root.kanan != null);
        traverseNodes(sb, "", pointerRight, root.kanan, false);

        return sb.toString();
    }

    private void traverseNodes(StringBuilder sb, String padding, String pointer, Node node,
            boolean hasRightSibling) {

        if (node != null) {

            sb.append("\n");
            sb.append(padding);
            sb.append(pointer);
            sb.append(node.data);

            StringBuilder paddingBuilder = new StringBuilder(padding);
            if (hasRightSibling) {
                paddingBuilder.append("│  ");
            } else {
                paddingBuilder.append("   ");
            }

            String paddingForBoth = paddingBuilder.toString();
            String pointerRight = "└──(ka)";
            String pointerLeft = (node.kanan != null) ? "├──(ki)" : "└──(ki)";

            traverseNodes(sb, paddingForBoth, pointerLeft, node.kiri, node.kanan != null);
            traverseNodes(sb, paddingForBoth, pointerRight, node.kanan, false);
        }
    }

    public void print(PrintStream os) {
        os.print(traversePreOrder(root));
    }
}
