/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tree_1;

/**
 *
 * @author djeq
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.add(10);
        tree.add(7);
        tree.add(8);
        tree.add(9);
        tree.add(6);
        tree.add(12);
        tree.add(11);
        tree.add(13);
        BinaryTreePrinter printer = new BinaryTreePrinter(tree.root);
        printer.print(System.out);
        tree.delete(7);
        printer.print(System.out);
    }

}
